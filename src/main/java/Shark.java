public class Shark extends Predator {
    @Override
    public String toString() {
        return "Shark " + getCharacter();
    }
}
