public class Wolf extends Predator {
    @Override
    public String toString() {
        return "Wolf с именем " + getName() + " с породой " + getBreed();
    }
}