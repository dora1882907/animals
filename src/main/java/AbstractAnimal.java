import java.time.LocalDate;
import java.util.Objects;

public abstract class AbstractAnimal implements Animal {
    protected String breed;
    protected String name;
    protected Double cost;
    protected String character;
    protected LocalDate birthDate;

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof AbstractAnimal)) {
            return false;
        }
        AbstractAnimal animal = (AbstractAnimal) o;
        if (!animal.name.equals(this.name)) {
            return false;
        }
        if (!animal.breed.equals(this.breed)) {
            return false;
        }
        if (!animal.cost.equals(this.cost)) {
            return false;
        }
        if (!animal.character.equals(this.character)) {
            return false;
        }
        if (!animal.birthDate.equals(this.birthDate)) {
            return false;
        }
        return true;
    }
    @Override
    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    @Override
    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    @Override
    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
