import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class SearchService {
    public List<String> findLeapYearNames(List<Animal> animals) {
        List<String> names = new ArrayList<>();
        for (int i = 0; i < animals.size(); i++) {
            Animal animal = animals.get(i);
            boolean flag = animal.getBirthDate().isLeapYear();
            if (flag) {
                names.add(animal.getName());
            }
        }
        return names;
    }

    public List<Animal> findOlderAnimal(List<Animal> animals, int N) {
        List<Animal> olderAnimals = new ArrayList<>();
        for (int i = 0; i < animals.size(); i++) {
            Animal animal = animals.get(i);
            int years = Period.between(animal.getBirthDate(), LocalDate.now()).getYears();
            if (years > N) {
                olderAnimals.add(animal);
            }
        }
        return olderAnimals;
    }

    public void findDuplicate(List<Animal> animals) {
        for (int i = 0; i < animals.size(); i++) {
            Animal animal = animals.get(i);
            for (int j = i + 1; j < animals.size(); j++) {
                Animal anotherAnimal = animals.get(j);
                if (animal.equals(anotherAnimal)) {
                    System.out.println("Дубликаты " + anotherAnimal);
                }

            }
        }
    }
}
