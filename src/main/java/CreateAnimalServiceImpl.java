import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CreateAnimalServiceImpl implements CreateAnimalService {
    public List<Animal> createAnimals(int N) {
        List<Animal> animals = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            Dog dog = new Dog();
            dog.setBreed("Ушастый " + i + ",");
            dog.setName("Шарик " + i);
            dog.setCharacter("ласковый " + i);
            dog.setCost(500.50);
            dog.setBirthDate(LocalDate.of(1999, 02, 20));
            System.out.println("Создали собаку " + dog + " c породой " + dog.getBreed() + "с харакетором " + dog.getCharacter());
            animals.add(dog);
        }
        return animals;
    }

    @Override
    public List<Animal> createAnimals() {
        List<Animal> animals = new ArrayList<>();
        int i = 0;

        do {
            i++;
            Shark shark = new Shark();
            shark.setName("Маша");
            shark.setBreed("Тигровая");
            shark.setCost(520.50);
            shark.setBirthDate(LocalDate.of(2000, 12, 11));
            if (i % 2 == 0)
                shark.setCharacter("Добрая");
            else shark.setCharacter("Злая");
            System.out.println("Была создана " + shark + " с характером " + shark.getCharacter());
            animals.add(shark);

        } while (i < 10);
        return animals;
    }
}
